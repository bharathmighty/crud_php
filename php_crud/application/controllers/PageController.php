<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PageController extends CI_Controller{

    public function index(){
        echo "I am home page";
    }

    public function about_us(){
        echo "I am about page";
    }

    public function blog($blogurl = '', $blogui = ''){
        $this->load->view('blogview');
        echo "$blogurl"." = ";
        echo "$blogui";
    }

    public function demo()
    {
        $this->load->model('StudentModel', 'sm');
        $data['title'] = $this->sm->demo();
        $data['body'] = "Welcome to my page";
        $this->load->view('demopage', $data);
    }


}
?>
