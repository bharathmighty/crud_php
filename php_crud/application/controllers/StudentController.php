<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StudentController extends CI_Controller{

    public function index(){
        $this->load->model('StudentModel','sm');
        $student = $this->sm->student_data();
        //$student_class = $this->sm->student_class();
        //$student = new StudentModel;
        //$student = $student->student_data();
        echo "student name : ".$student;
        echo "<br>";
        //echo "student class : ". "$student_class";
    }

    public function show($id){
        echo $id;
        $this->load->model('StudentModel','sm');
        $selected_student = $this->sm->student_show($id);
        echo $selected_student;
    }
}

?>
